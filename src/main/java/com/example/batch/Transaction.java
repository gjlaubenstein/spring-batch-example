package com.example.batch;

public class Transaction {
    private Integer id;
    private Integer accountId;
    private Double amount;
    private Boolean paid;

    public Transaction() {
    }

    public Transaction(Integer id, Integer accountId, Double amount, Boolean paid) {
        this.id = id;
        this.accountId = accountId;
        this.amount = amount;
        this.paid = paid;
    }

    public Integer getId() {
        return id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public Double getAmount() {
        return amount;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }
}
