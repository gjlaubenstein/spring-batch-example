package com.example.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.item.SimpleChunkProcessor;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.function.FunctionItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.AggregatorFactoryBean;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import javax.batch.api.chunk.listener.ItemWriteListener;
import javax.sql.DataSource;

@Configuration
public class BatchConfiguration {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private ExampleItemWriter exampleItemWriter;

    @Autowired
    private JobIncrementor jobIncrementor;

    @Bean
    public Job exampleJob() {
        return this.jobBuilderFactory.get("example1")
                .incrementer(jobIncrementor)
                .start(exampleStep())
                .build();
    }

    @Bean
    public Step exampleStep() {
        return this.stepBuilderFactory.get("mark transactions")
                .<Transaction, Transaction>chunk(10)
                .reader(this.exampleReader())
                .processor(this.exampleProcessor())
                .writer(this.exampleItemWriter)
                .build();
    }

    @Bean
    public ItemReader<Transaction> exampleReader() {
        JdbcCursorItemReader<Transaction> objectJdbcCursorItemReader = new JdbcCursorItemReader<>();
        objectJdbcCursorItemReader.setSql("SELECT id, accountId, amount, paid FROM transactions");
        objectJdbcCursorItemReader.setDataSource(this.dataSource);
        objectJdbcCursorItemReader.setRowMapper(new BeanPropertyRowMapper<>(Transaction.class));
        return objectJdbcCursorItemReader;
    }

    @Bean
    public ItemProcessor<Transaction, Transaction> exampleProcessor() {
        return new FunctionItemProcessor<>((transaction) -> {
            return transaction;
        });
    }

}
