package com.example.batch;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExampleItemWriter implements ItemWriter<Transaction> {
    @Autowired
    private JdbcOperations jdbcOperations;

    private final static String sql = "UPDATE transactions SET paid = true WHERE id = ?";

    @Override
    public void write(List<? extends Transaction> items) {
        List<Object[]> batchArgs = items.stream()
                .map((transaction) -> new Object[]{transaction.getId()})
                .collect(Collectors.toList());
        jdbcOperations.batchUpdate(sql, batchArgs);
    }
}
